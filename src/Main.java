public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe");
        contact1.setContactNumber("+639152468596");
        contact1.setAddress("Quezon City");


        Contact contact2 = new Contact("Jane Doe");
        contact2.setContactNumber("+639162148573");
        contact2.setAddress("Caloocan City");


        phonebook.addContact(contact1);
        phonebook.addContact(contact2);


        phonebook.displayContacts();
    }
}

